FROM ros:noetic-robot
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    nano \
    tmux \
    ros-noetic-rqt \
    ros-noetic-rviz \
    ros-noetic-turtlebot3-teleop \
    ros-noetic-rqt-common-plugins
