# SY31 - Docker image
Docker image fitted with the right packages for SY31 + compose file to run smoothly. Courtesy of @ykumaras & @khimayan.

## Run the image

1. Clone this repository: `git clone https://gitlab.utc.fr/SY31/docker -- sy31-docker`
2. Go inside it: `cd sy31-docker`
3. Start the service in the background: `docker compose up -d`
4. Open new terminals: `docker compose run ros`

By default, a bash history will be synced with the one in this repo and a `workspace` folder will be created to export your work.

## Update the image
A Gitlab CI is setup to build and push an image to [registry.gitlab.utc.fr/sy31/docker](registry.gitlab.utc.fr/sy31/docker) with the lastest `Dockerfile`.

To update the image, simply open a merge request and upon acception, the image will automatically be updated.
